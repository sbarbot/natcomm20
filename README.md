# README #

This repository contains Matlab scripts to compute the displacement induced by plastic strain confined in a vertical, semi-infinite cuboid. Application for crustal dynamics can be found in 

Barbot S., Flow distribution beneath the California margin, Nature Communications, 2020.

### What is this repository for? ###

* The repository contains Matlab script to evaluate deformation (displacements).
* An example driver is provided.

### How do I get set up? ###

* Download the Matlab scripts in the same folder.
* Run the driver.

### Who do I talk to? ###

* Repository owner (sbarbot@usc.edu).
