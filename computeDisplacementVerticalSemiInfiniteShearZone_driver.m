%% % % % % % % % % % % % % % % % % % % % % % % % % % %
%
% EXAMPLE CALCULATION FOR DEFORMATION ASSOCIATED WITH
% A PLASTICALLY DEFORMING VERTICAL SEMI-INFINITE
% CUBOID.
%
% AUTHOR: Sylvain Barbot (sbarbot@usc.edu)
% % % % % % % % % % % % % % % % % % % % % % % % % % %%

% prescribe strain in the shear-zone-centric coordinate system
epsv11=0e-6;
epsv12=1e-6;
epsv13=0e-6;
epsv22=0e-6;
epsv23=0e-6;
epsv33=0e-6;

% dimension
L=1e3; % length along strike
T=1e3; % thickness

% position
q1=-1e3;
q2=0;
q3=1e3; % depth

% orientation
theta=30;

% elastic structure
nu=0.25; % Poisson's ratio nu=l/(2*(l+mu))

% observation points
N=64;
dx1=0.1e3;
dx2=0.1e3;
x1=repmat((-N/2:(N/2)-1)'*dx1,1,N)+0.01e3;
x2=repmat((-N/2:(N/2)-1)*dx2,N,1)+0.01e3;
x3=0*x2;

fprintf('Gauss-Legendre solution\n');
[n.u1,n.u2,n.u3]=computeDisplacementVerticalSemiInfiniteShearZoneSurfaceGauss( ...
    x1,x2,q1,q2,q3,L,T,theta,epsv11,epsv12,epsv13,epsv22,epsv23,epsv33,nu,'N',11,'precision',0.05);

%% plot displacements

figure(1);clf;

subplot(2,2,1);cla;hold on;
pcolor(x2/1e3,x1/1e3,n.u1),shading flat;
plot(q2/1e3,q1/1e3,'+')
h=colorbar();
ylabel(h,'u_1');
axis equal tight
box on, grid on
xlabel('x_2 (km)');
ylabel('x_1 (km)');
title('Component u1');

subplot(2,2,2);cla;hold on;
pcolor(x2/1e3,x1/1e3,n.u2),shading flat;
plot(q2/1e3,q1/1e3,'+')
h=colorbar();
ylabel(h,'u_2');
axis equal tight
box on, grid on
set(gca,'TickDir','Out')
xlabel('x_2 (km)');
ylabel('x_1 (km)');
title('Component u2');

subplot(2,2,3);cla;hold on;
pcolor(x2/1e3,x1/1e3,-n.u3),shading flat;
plot(q2/1e3,q1/1e3,'+')
h=colorbar();
ylabel(h,'u_3');
axis equal tight
box on, grid on
set(gca,'TickDir','Out')
xlabel('x_2 (km)');
ylabel('x_1 (km)');
title('Component u3');

subplot(2,2,4);cla;hold on
axis equal tight
down=4;
pcolor(x2/1e3,x1/1e3,-n.u3),shading flat;
quiver(x2(1:down:end,1:down:end)/1e3,x1(1:down:end,1:down:end)/1e3, ...
    n.u2(1:down:end,1:down:end),n.u1(1:down:end,1:down:end));
plot(q2/1e3,q1/1e3,'+')
h=colorbar();
ylabel(h,'Displacement');
axis equal tight
set(gca,'clim',[-1 1]*max(abs(get(gca,'clim'))));
set(gca,'TickDir','Out')
box on, grid on
xlabel('x_2 (km)');
ylabel('x_1 (km)');


