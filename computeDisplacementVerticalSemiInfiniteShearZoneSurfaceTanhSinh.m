function [u1,u2,u3]=computeDisplacementVerticalSemiInfiniteShearZoneSurfaceTanhSinh( ...
    x1,x2,q1,q2,q3,L,T,theta, ...
    epsv11,epsv12,epsv13,epsv22,epsv23,epsv33,nu,varargin)
% function COMPUTEDISPLACEMENTKERNELVERTICALSEMIINFINITESHEARZONESURFACETANHSINH
% computes the surface displacement field associated with deforming volume
% elements using the double exponential quadrature considering the 
% following geometry:
%
%                      N (x1)
%                     /
%                    /| strike (theta)
%                   @----------------- E (x2)
%                   :
%                   |     +--------------------------+
%                   :    /                          /|
%                   |   /                          / |
%            Z (x3) :  /                          / s|
%                   | /                          / s .
%                   :/                          / e  .
%        q1,q2,q3 ->+--------------------------+ n   .
%                  /      l e n g t h  (L)    / k
%                 /                          / c
%                /                          / i
%               /                          / h
%              /                          / t
%             +--------------------------+
%             |                          |
%             |                          |
%             |                          |
%             .                          .
%             .                          .
%             .                          .
%             .                          .
%
% Input:
% x1, x2             north and east coordinates of the observation points
% q1, q2, q3         north, east and depth coordinates of the volume element
% L, T               length and thickness of the volume element
% theta (degree)     strike angle from north (from x1) of the volume element
% epsvij             source strain component ij in the volume element in the 
%                    reference system tied to the volume element.
% nu                 Poisson's ratio in the half space.
%
% Options:
% 'N',integer        number of integration points for the Gauss-Legendre
%                    quadrature [15].
% 'precision'        controls the number of points used for the
%                    double-exponential integration [0.001].
% 'bound'            truncation of the double-exponential integral [3.5].
%
% Output:
% u1                 displacement component in the north direction,
% u2                 displacement component in the east direction,
% u3                 displacement component in the down direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - Feb 5, 2019, Los Angeles.

% process optional input
p = inputParser;
p.addParameter('N',15,@validateN);
p.addParameter('precision',0.001,@validatePrecision);
p.addParameter('bound',3.5,@validateBound);
p.parse(varargin{:});
optionStruct = p.Results;

% Lame parameter
lambda=2*nu/(1-2*nu);

% isotropic strain
epsvkk=epsv11+epsv22+epsv33;

% array size
s=size(x1);

% rotate to the shear-zone-centric system of coordinates
t1= (x1-q1)*cosd(theta)+(x2-q2)*sind(theta);
x2=-(x1-q1)*sind(theta)+(x2-q2)*cosd(theta);
x1=t1;

% Green's functions
r=@(y1,y2,y3) sqrt((x1-y1).^2+(x2-y2).^2+y3.^2);

% numerical solution with tanh/sinh quadrature
h=optionStruct.precision;
n=fix(1/h*optionStruct.bound);

u1=zeros(s);
u2=zeros(s);
u3=zeros(s);

% Double-Exponential integration over horizontal faces;
% integration over (-1,1)
for j=-n:n
    wj=(0.5*h*pi*cosh(j*h))./(cosh(0.5*pi*sinh(j*h))).^2;
    xj=tanh(0.5*pi*sinh(j*h));
    for k=-n:n
        wk=(0.5*h*pi*cosh(k*h))./(cosh(0.5*pi*sinh(k*h))).^2;
        xk=tanh(0.5*pi*sinh(k*h));
        u1=u1+wj*wk*IU1h(xk,xj);
        u2=u2+wj*wk*IU2h(xk,xj);
        u3=u3+wj*wk*IU3h(xk,xj);
    end
end

% Double-Exponential integration over vertical faces
% integration over (0,infinity)
for j=-n:n
    wj=0.5*h*pi*cosh(j*h).*exp(0.5*pi*sinh(j*h));
    xj=exp(0.5*pi*sinh(j*h));
    for k=-n:n
        wk=(0.5*h*pi*cosh(k*h))./(cosh(0.5*pi*sinh(k*h))).^2;
        xk=tanh(0.5*pi*sinh(k*h));
        u1=u1+wj*wk*IU1v(xk,xj);
        u2=u2+wj*wk*IU2v(xk,xj);
        u3=u3+wj*wk*IU3v(xk,xj);
    end
end

% rotate displacement field to reference system of coordinates
t1=u1*cosd(theta)-u2*sind(theta);
u2=u1*sind(theta)+u2*cosd(theta);
u1=t1;


    function d = G11(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr+1./lr+(x1-y1).^2./lr.^3+(3-4*nu)*(x1-y1).^2./lr.^3 ...
            +4*(1-2*nu)*(1-nu)*(lr.^2-(x1-y1).^2+lr.*y3)./(lr.*(lr+y3).^2));
    end
    function d = G12(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=(x1-y1).*(x2-y2)/(16*pi*(1-nu)).*( ...
            1./lr.^3+(3-4*nu)./lr.^3 ...
            -4*(1-2*nu)*(1-nu)./(lr.*(lr+y3).^2) ...
            );
    end
    function d = G13(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=(x1-y1)/(16*pi*(1-nu)).*( ...
            (-y3)./lr.^3+(3-4*nu)*(-y3)./lr.^3 ...
            +4*(1-2*nu)*(1-nu)./(lr.*(lr+y3)) ...
            );
    end
    function d = G21(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=(x1-y1).*(x2-y2)/(16*pi*(1-nu)).*( ...
            1./lr.^3+(3-4*nu)./lr.^3 ...
            -4*(1-2*nu)*(1-nu)./(lr.*(lr+y3).^2) ...
            );
    end
    function d = G22(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr+1./lr+(x2-y2).^2./lr.^3 ...
            +(3-4*nu)*(x2-y2).^2./lr.^3 ...
            +4*(1-2*nu)*(1-nu)*(lr.^2-(x2-y2).^2+lr.*(y3))./(lr.*(lr+y3).^2)...
            );
    end
    function d = G23(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            +(4-4*nu)*(-y3)./lr.^3 ...
            +4*(1-2*nu)*(1-nu)./(lr.*(lr+y3)) ...
            );
    end
    function d = G31(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=(x1-y1)/(16*pi*(1-nu)).*( ...
            +(4-4*nu)*(-y3)./lr.^3 ...
            -4*(1-2*nu)*(1-nu)./(lr.*(lr+y3)) ...
            );
    end
    function d = G32(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=(x2-y2)/(16*pi*(1-nu)).*( ...
            (-y3)./lr.^3+(3-4*nu)*(-y3)./lr.^3 ...
            -4*(1-2*nu)*(1-nu)./(lr.*(lr+y3)) ...
            );
    end
    function d = G33(y1,y2,y3)
        lr=r(y1,y2,y3);
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr+(5-12*nu+8*nu^2)./lr+(-y3).^2./lr.^3 ...
            +((3-4*nu)*(+y3).^2)./lr.^3 ...
            );
    end

    % integration over (-1,1) for the horizontal faces
    function u = IU1h(x,y)
        % function IU1 is the integrand for displacement component u1
        u=zeros(s);
        if epsv13 ~= 0
            u=u+2*epsv13*L*T/4*(-G11((1+x)*L/2,y*T/2,q3));
        end
        if epsv23 ~= 0
            u=u+2*epsv23*L*T/4*(-G21((1+x)*L/2,y*T/2,q3));
        end
        if epsv33 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv33)*L*T/4*(-G31((1+x)*L/2,y*T/2,q3));
        end
    end

    function u = IU2h(x,y)
        % function IU2 is the integrand for displacement component u1
        u=zeros(s);
        if epsv13 ~= 0
            u=u+2*epsv13*L*T/4*(-G12((1+x)*L/2,y*T/2,q3));
        end
        if epsv23 ~= 0
            u=u+2*epsv23*L*T/4*(-G22((1+x)*L/2,y*T/2,q3));
        end
        if epsv33 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv33)*L*T/4*(-G32((1+x)*L/2,y*T/2,q3));
        end
    end

    function u = IU3h(x,y)
        % function IU3 is the integrand for displacement component u3
        u=zeros(s);
        if epsv13 ~= 0
            u=u+2*epsv13*L*T/4*(-G13((1+x)*L/2,y*T/2,q3));
        end
        if epsv23 ~= 0
            u=u+2*epsv23*L*T/4*(-G23((1+x)*L/2,y*T/2,q3));
        end
        if epsv33 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv33)*L*T/4*(-G33((1+x)*L/2,y*T/2,q3));
        end
    end

    % integration over (0,infinity) for the vertical faces
    function u = IU1v(x,y)
        % function IU1 is the integrand for displacement component u1
        u=zeros(s);
        if epsv11 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv11)*T/2*(G11(L,x*T/2,y+q3)-G11(0,x*T/2,y+q3));
        end
        if epsv12 ~= 0
            u=u+2*epsv12*T/2*(G21(L,x*T/2,y+q3)-G21(0,x*T/2,y+q3)) ...
               +2*epsv12*L/2*(G11((1+x)*L/2,T/2,y+q3)-G11((1+x)*L/2,-T/2,y+q3));
        end
        if epsv13 ~= 0
            u=u+2*epsv13*T/2*(G31(L,x*T/2,y+q3)-G31(0,x*T/2,y+q3));
        end
        if epsv22 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv22)*L/2*(G21((1+x)*L/2,T/2,y+q3)-G21((1+x)*L/2,-T/2,y+q3));
        end
        if epsv23 ~= 0
            u=u+2*epsv23*L/2*(G31((1+x)*L/2,T/2,y+q3)-G31((1+x)*L/2,-T/2,y+q3));
        end
    end

    function u = IU2v(x,y)
        % function IU2 is the integrand for displacement component u1
        u=zeros(s);
        if epsv11 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv11)*T/2*(G12(L,x*T/2,y+q3)-G12(0,x*T/2,y+q3));
        end
        if epsv12 ~= 0
            u=u+2*epsv12*T/2*(G22(L,x*T/2,y+q3)-G22(0,x*T/2,y+q3)) ...
               +2*epsv12*L/2*(G12((1+x)*L/2,T/2,y+q3)-G12((1+x)*L/2,-T/2,y+q3));
        end
        if epsv13 ~= 0
            u=u+2*epsv13*T/2*(G32(L,x*T/2,y+q3)-G32(0,x*T/2,y+q3));
        end
        if epsv22 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv22)*L/2*(G22((1+x)*L/2,T/2,y+q3)-G22((1+x)*L/2,-T/2,y+q3));
        end
        if epsv23 ~= 0
            u=u+2*epsv23*L/2*(G32((1+x)*L/2,T/2,y+q3)-G32((1+x)*L/2,-T/2,y+q3));
        end
    end

    function u = IU3v(x,y)
        % function IU3 is the integrand for displacement component u3
        u=zeros(s);
        if epsv11 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv11)*T/2*(G13(L,x*T/2,y+q3)-G13(0,x*T/2,y+q3));
        end
        if epsv12 ~= 0
            u=u+2*epsv12*T/2*(G23(L,x*T/2,y+q3)-G23(0,x*T/2,y+q3)) ...
               +2*epsv12*L/2*(G13((1+x)*L/2,T/2,y+q3)-G13((1+x)*L/2,-T/2,y+q3));
        end
        if epsv13 ~= 0
            u=u+2*epsv13*T/2*(G33(L,x*T/2,y+q3)-G33(0,x*T/2,y+q3));
        end
        if epsv22 ~= 0 || epsvkk ~= 0
            u=u+(lambda*epsvkk+2*epsv22)*L/2*(G23((1+x)*L/2,T/2,y+q3)-G23((1+x)*L/2,-T/2,y+q3));
        end
        if epsv23 ~= 0
            u=u+2*epsv23*L/2*(G33((1+x)*L/2,T/2,y+q3)-G33((1+x)*L/2,-T/2,y+q3));
        end
    end

end

function p = validatePrecision(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidPrecision'));
end
p = true;
end

function p = validateBound(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidBound'));
end
p = true;
end

function p = validateN(x)
if ~(x > 0)
    error('MATLAB:invalid','invalid number of integration points');
end
p = true;
end


